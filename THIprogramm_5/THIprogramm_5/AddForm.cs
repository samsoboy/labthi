﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THIprogramm_3;
using THIprogramm_4logic;

namespace THIprogramm_5

{
    public partial class AddForm : Form
    {
        private MainForm form1;
        
        public AddForm(MainForm form)
        {
            InitializeComponent();
            form1 = form;
        }
        
        private void OKbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClasscomboBox1.Text == "MachineGun")
                {
                    MachineGun weapon = new MachineGun();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);

                }
                else if (ClasscomboBox1.Text == "ShotGun")
                {
                    Shotgun weapon = new Shotgun();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (ClasscomboBox1.Text == "Edged")
                {
                    Edged weapon = new Edged();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (ClasscomboBox1.Text == "Trauma")
                {
                    Trauma weapon = new Trauma();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (ClasscomboBox1.Text == "Pointed")
                {
                    Pointed weapon = new Pointed();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
            }
            catch (Exception) { }
            this.Dispose();

        }

        private void Cancelbutton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void AddForm_Load(object sender, EventArgs e)
        {

        }

        private void ClasscomboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
