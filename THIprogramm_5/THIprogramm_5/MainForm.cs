﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THIprogramm_3;
using THIprogramm_4logic;

namespace THIprogramm_5
{
    public partial class MainForm : Form
    {
        public Division mainDivision = DivisionFactory.CreateDivision();

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WeaponlistBox1.Items.Add(mainDivision.WeaponList[0].Name + " - " + mainDivision.WeaponList[0].Price);
            this.WeaponlistBox1.Items.Add(mainDivision.WeaponList[1].Name + " - " + mainDivision.WeaponList[1].Price);
            
        }

        private void Addbutton_Click(object sender, EventArgs e)
        {
            AddForm AddWeapon = new AddForm(this);
            AddWeapon.Show();
        }

        private void Editbutton_Click(object sender, EventArgs e)
        {
            EditForm EditWeapon = new EditForm(this);
            EditWeapon.Show();
        }

        private void Deletebutton_Click(object sender, EventArgs e)
        {
            mainDivision.WeaponList.RemoveAt(WeaponlistBox1.SelectedIndex);
            WeaponlistBox1.Items.Remove(WeaponlistBox1.SelectedItem);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void WeaponlistBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            label1.Text = "Общая цена: " + DivisionCalculator.GetTotalPrice(mainDivision);
        }

        public ListBox WeaponListBox
        {
            get { return WeaponlistBox1; }
            set { WeaponlistBox1 = value; }
        }
    }
}
